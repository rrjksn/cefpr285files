This repository tracks the cef changes required for PR285 shared textures.
Its difficult updating the PR to a new cef release as the PR changes have to be disentangled from the chromium and cef updates
The chromium patches made by viz_osr_2575.patch are a particular problem as many of the chromium files are patched by standard cef and the there are additional patches to them from the PR.

The repository is baselined on cef release 4183 which used chromium 85.0.4183.121

There are three branches in the repository

1. Chromium_files
	This branch contains only the unmodified chromium files which are going to be patched by cef and the PR. They are in the chromium/src folder.
	The Chromium folder also contains the file Vizpatchedfiles.txt which lists the required Chromium 
2. cef
	This branch contains the complete standard cef code in the cef folder. The chromium/src files are updated as patched from standard cef.
3. PR285
	The cef files are updated as required for the PR. The chromium/src files are updated as patched from the PR.

Its intended that the process to update to a new cef/chromium version is:

1. Switch to Chromium_files branch. Update the chromium files from the required version. Any changes to them will be obvious.
	I've been using WSL to copy across the files from a chromium pull. EG
	`richard@IGPC-88:/mnt/d/cefbuild4183/chromium/src$ cat /mnt/d/CefPR285files/chromium/Vizpatchedfiles.txt  | cpio -pvud /mnt/d/CefPR285files/chromium/src`
2. Commit the changes to the Chromium_files branch..
3. Switch to cef branch. Merge the Chromium_files branch into it. Any issues will be obvious.
4. Copy the required cef version over the cef folder. Staging all and then unstaging all will leave just the files that are actually changed.
5. Do a build of the required cef version. This will patch the chromium files. Copy across the patched files as per step one. 
6. Commit the changes to the cef branch.
7. Switch to the PR285 branch. Merge the cef branch into it. Looking at the changes should make it a lot easier to see what needs to be done to make it build correctly.
